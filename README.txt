Monotone BART

This is a bare bones release of Monotone BART
(Chipman, George, McCulloch, and Shively) 
prior to release of an R package.

To install:
Go to the code subdirecty and follow the README.txt there.


Note: the basic function is ``monbart''.
No documentation is given, but the arguments follow bart in BayesTree
except some of the prior defaults are different as discussed in the paper.
``rpmonbart'' runs MCMC chains in parallel.
